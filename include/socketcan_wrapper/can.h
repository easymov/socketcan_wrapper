/**
 *  @file can.h
 *  @author Easymov Robotics
 *  @date 2017-03
 */

#ifndef CAN_H
#define CAN_H

#include "socketcan_wrapper/can_frame.h"
#include "socketcan_wrapper/socket_handler.h"
#include <ros/time.h>
#include <stdexcept>
#include <memory>

namespace socketcan_wrapper
{
/**
 * This class is the interface to a CAN socket.
 */
class Can
{
public:
  /** Base class of all exceptions thrown by this class */
  struct CanException : public std::runtime_error
  {
    CanException(const std::string& msg) : std::runtime_error(msg)
    {
    }
  };

  /** Exception thrown when an operation is not possible beacause the CAN
   * socket is not connected */
  struct CanDisconnectedException : public CanException
  {
    CanDisconnectedException() : CanException("CAN socket disconnected")
    {
    }
  };

  /** Exception thrown when the connection to the CAN socket was unsuccessful
   */
  struct CanConnectionException : public CanException
  {
    CanConnectionException(const std::string& msg = "Cannot connect to CAN device") : CanException(msg)
    {
    }
  };

  /** Exception thrown when the CAN socket cannot be created */
  struct SocketCreationException : public CanConnectionException
  {
    SocketCreationException() : CanConnectionException("Cannot create CAN socket")
    {
    }
  };

  /** Exception thrown when the given device name is too long */
  struct DeviceNameException : public CanConnectionException
  {
    DeviceNameException() : CanConnectionException("CAN device name too long")
    {
    }
  };

  /** Exception thrown when writing to the CAN socket was unsuccessful */
  struct WriteException : public CanException
  {
    WriteException(const std::string& msg) : CanException(std::string("Write to CAN socket failed: ") + msg)
    {
    }
  };

  struct ReadException : public CanException
  {
    ReadException(const std::string& msg = "Read to CAN socket failed") : CanException(msg)
    {
    }
  };

  /** Exception thrown when waiting for data on CAN socket was unsuccessful */
  struct ReadWaitException : public ReadException
  {
    ReadWaitException() : ReadException("Wait for CAN socket reading failed")
    {
    }
  };

  /** Exception thrown when reading on CAN socket exceed alloted time */
  struct ReadTimeoutException : public ReadException
  {
    ReadTimeoutException() : ReadException("Read to CAN socket timeout occured")
    {
    }
  };

  /** Exception thrown a frame read on CAN socket is incomplete */
  struct IncompleteReadException : public ReadException
  {
    IncompleteReadException() : ReadException("Imcomplete CAN frame read")
    {
    }
  };

public:
  /** Default constructor */
  Can();

  /**
   * Open a CAN socket connected to the interface provided.
   *
   * @param device is the name of the interface to connect to
   * @throw SocketCreationException
   * @throw CanConnectionException
   * @throw DeviceNameException
   */
  Can(const std::string& device);

  /**
   * Listen to the CAN socket and wait until a message is received or the
   * timeout is expired.
   *
   * @param[out] frame will be filled with the received frame
   * @param[in] timeout maximum time to wait before returning [s]
   * max timeout is 2^31 ms (24.86 days)
   * @return the time recorded just after receiving the message
   * @throw CanDisconnectedException
   * @throw ReadTimeoutException
   * @throw ReadWaitException
   * @throw ReadException
   * @throw IncompleteReadException
   */
  ros::Time receive(CanFrame& frame, double timeout);

  /**
   * Listen to the CAN socket and wait until a message is received.
   *
   * @param[out] frame will be filled with the received frame
   * @return the time recorded just after receiving the message
   * @throw CanDisconnectedException
   * @throw ReadTimeoutException
   * @throw ReadWaitException
   * @throw ReadException
   * @throw IncompleteReadException
   */
  ros::Time receive(CanFrame& frame);

  /**
   * Send a CAN frame to the CAN socket.
   *
   * @param frame is the frame to send
   * @return the time recorded just after sending message
   * @throw CanDisconnectedException
   * @throw WriteException
   */
  ros::Time send(const CanFrame& frame);

private:
  ros::Time receive(CanFrame& frame, std::unique_ptr<timeval> pt);

private:
  std::shared_ptr<SocketHandler> socket_handler_; /** Will close the socket on
                                                    destruction */
};
}  // namespace socketcan_wrapper

#endif
