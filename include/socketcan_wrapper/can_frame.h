/**
 *  @file can_frame.h
 *  @author Easymov Robotics
 *  @date 2017-03
 */

#ifndef CAN_FRAME_H
#define CAN_FRAME_H

#include <cstddef>
#include <fstream>
#include <linux/can.h>

namespace socketcan_wrapper
{
/**
 * Wrap a basic CAN frame structure.
 */
struct CanFrame : public can_frame
{
  /**
   * Set all CAN frame fields to null and
   * set CAN identifier to the one provided.
   * @param can_id is the CAN identifier to set
   */
  CanFrame(canid_t can_id = 0);

  /** @return the CAN frame size (in bytes) */
  static std::size_t Size();
};

/** Allow to stream CAN frames as string */
std::ostream& operator<<(std::ostream& os, const CanFrame& frame);

}  // namespace socketcan_wrapper

#endif
