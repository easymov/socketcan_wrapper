/**
 *  @file socket_handler.h
 *  @author Easymov Robotics
 *  @date 2017-03
 */

#ifndef SOCKET_HANDLER_H
#define SOCKET_HANDLER_H

#include <unistd.h>

namespace socketcan_wrapper
{
/**
 * This class take a file descriptor and close it
 * on destruction.
 */
class SocketHandler
{
public:
  /**
   * Constructor.
   * @param socket_id is the file descriptor
   */
  SocketHandler(int socket_id = -1) : socket_id_(socket_id)
  {
  }

  /** Close the file descriptor. */
  ~SocketHandler()
  {
    if (is_valid())
    {
      close(socket_id_);
    }
  }

  /** Integer cast operator to retrieve the file descriptor */
  operator int() const
  {
    return socket_id_;
  }

  /** @return true if file descriptor is valid, false otherwise*/
  bool is_valid() const
  {
    return (socket_id_ != -1);
  }

private:
  int socket_id_; /** the file descriptor */
};
}  // namespace socketcan_wrapper

#endif
