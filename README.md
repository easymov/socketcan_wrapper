# socketcan_wrapper

## Overview

C++ wrapper library around socketcan.
`ros::Time::now` is used to time message reception.

## Usage

```cpp
Can canbus("can0"); // open can0 socketcan

CanFrame frame;
ros::Time timepoint;

timepoint = canbus.receive(frame, 1.0); // receive with timeout
timepoint = canbus.receive(frame); // receive without timeout
timepoint = canbus.send(frame);
```