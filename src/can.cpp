/**
 *  @file can.cpp
 *  @author Easymov Robotics
 *  @date 2017-03
 */

#include <net/if.h>     // IFNAMSIZ
#include <sys/ioctl.h>  // SIOCGIFINDEX

#include "socketcan_wrapper/can.h"

#ifndef AF_CAN
#define AF_CAN 29
#endif

namespace socketcan_wrapper
{
Can::Can() : socket_handler_(std::make_shared<SocketHandler>(-1))
{
}

Can::Can(const std::string& device)
{
  socket_handler_ = std::make_shared<SocketHandler>(socket(AF_CAN, SOCK_RAW, CAN_RAW));

  if (!socket_handler_->is_valid())
    throw SocketCreationException();

  if (device.size() > IFNAMSIZ)
    throw DeviceNameException();

  struct ifreq ifr;
  strncpy(ifr.ifr_name, device.c_str(), IFNAMSIZ);

  if (ioctl(*socket_handler_.get(), SIOCGIFINDEX, &ifr) < 0)
  {
    throw CanConnectionException();
  }

  struct sockaddr_can addr;
  addr.can_family = AF_CAN;
  addr.can_ifindex = ifr.ifr_ifindex;

  if (bind(*socket_handler_.get(), reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0)
  {
    throw CanConnectionException();
  }

  if (!socket_handler_->is_valid())
    throw CanConnectionException();
}

ros::Time Can::receive(CanFrame& frame, double timeout)
{
  std::unique_ptr<timeval> pt;

  if (timeout != -1)
  {
    pt = std::unique_ptr<timeval>(new timeval);
    pt->tv_sec = int(timeout);
    pt->tv_usec = (int(timeout * 1000.0) % 1000) * 1000;
  }

  return receive(frame, std::move(pt));
}

ros::Time Can::receive(CanFrame& frame)
{
  return receive(frame, std::unique_ptr<timeval>());
}

ros::Time Can::receive(CanFrame& frame, std::unique_ptr<timeval> pt)
{
  if (!socket_handler_->is_valid())
    throw CanDisconnectedException();

  fd_set read_file;
  FD_ZERO(&read_file);
  FD_SET(*socket_handler_.get(), &read_file);

  int ready = select((*socket_handler_.get()) + 1, &read_file, 0, 0, pt.get());

  ros::Time now = ros::Time::now();

  if (ready == 0)
    throw ReadTimeoutException();
  if (ready < 0)
    throw ReadWaitException();

  ssize_t read_bytes = read(*socket_handler_.get(), &frame, CanFrame::Size());

  if (read_bytes < 0)
    throw ReadException();
  if (read_bytes == 0)
    throw CanDisconnectedException();
  if (static_cast<size_t>(read_bytes) < CanFrame::Size())
    throw IncompleteReadException();

  return now;
}

ros::Time Can::send(const CanFrame& frame)
{
  if (!socket_handler_->is_valid())
    throw CanDisconnectedException();

  int written_bytes = write(*socket_handler_.get(), &frame, CanFrame::Size());
  ros::Time now = ros::Time::now();

  if (written_bytes == -1)
    throw WriteException(strerror(errno));

  return now;
}

}  // namespace socketcan_wrapper

#undef AF_CAN
