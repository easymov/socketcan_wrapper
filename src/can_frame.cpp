/**
 *  @file can_frame.cpp
 *  @author Easymov Robotics
 *  @date 2017-03
 */

#include "socketcan_wrapper/can_frame.h"
#include <cstring>  // memset
#include <iomanip>  // setfill, dec, hex, setw

namespace socketcan_wrapper
{
CanFrame::CanFrame(canid_t n_can_id)
{
  std::memset(this, 0, sizeof(CanFrame));
  can_id = n_can_id;
}

size_t CanFrame::Size()
{
  return sizeof(struct can_frame);
}

std::ostream& operator<<(std::ostream& os, const CanFrame& f)
{
  /* Header */

  os << "id=" << f.can_id << " dlc=" << static_cast<int>(f.can_dlc) << " data=.";

  /* Decimal format */

  for (size_t i = 0; i < f.can_dlc; ++i)
  {
    os << static_cast<int>(f.data[i]) << ".";
  }

  /* Hexadecimal format */

  os << "(hexa)." << std::setfill('0') << std::hex;

  for (size_t i = 0; i < f.can_dlc; ++i)
  {
    os << std::setw(2) << static_cast<int>(f.data[i]) << ".";
  }

  os << std::setfill(' ') << std::dec;

  /* ASCII format */

  os << "(ascii): ";

  for (size_t i = 0; i < f.can_dlc; ++i)
  {
    // ascii[integer] or --[integer]
    if ((f.data[i] < 32) || (f.data[i] > 126))
    {
      os << static_cast<char>(169);
    }
    else
    {
      os << f.data[i];
    }
  }

  os << std::endl;

  return os;
}

}  // namespace socketcan_wrapper
